CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 
INTRODUCTION
------------

The Mappoint Module allows a user to create a responsive map to
display on the front end. It can only output using the tokens
functionality.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/shaunfrisbee/2468361

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2468361
   
REQUIREMENTS
------------

This module requires the following modules:

 * Entity API (https://www.drupal.org/project/entity)
 * Token (https://www.drupal.org/project/token)
 * Jquery Colorpicker (https://www.drupal.org/project/jquery_colorpicker)
 * Token Filter
   (https://ftp.drupal.org/files/projects/token_filter-7.x-1.1.tar.gz)

RECOMMENDED MODULES
-------------------

 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered with
   markdown.

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
