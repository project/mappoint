/**
 * @file
 * Zoom and click functionality of the map.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.mappoint = {
    attach: function (context, settings) {

      if (typeof (settings.mappoint) == 'undefined') {
        return;
      }

      /*
       * Pull the variables from the settings.
       */
      var view = settings.mappoint.view;
      var map = settings.mappoint.map;
      var overrides = settings.mappoint.overrides;
      var emap = $('#map_' + map.id);
      var usmap = $('#usmap_' + map.id);
      
      // The int value of click_action is coming as a string.
      map.click_action = parseInt(map.click_action, 10);
      
      /*
       *  Define the default functionality of the map.
       */
      if (view === 'normal' || view === 'map') {
        // Add functionality for maps that have the states linked.
        if (map.click_action === 1) {
          // State click event.
          emap.find('path').click(function (e) {
            // Get the state from the ID.
            var state = this.id.replace(/^US-/, '');
            // Locate the URL for the state clicked.
            for (var i in overrides) {
              // Match the state with the override.
              if (overrides[i].state === state) {
                // Ensure the link is not null.
                if (overrides[i].link !== null) {
                  // Redirect the window.
                  window.location.href = overrides[i].link;
                }
                break;
              }
            }
          });
        }
        else {
          // Overlay close button functionality.
          emap.find('a.overlay-close').click(function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $('#map_' + map.id + '_overlay_drop_shadow.active,#map_' + map.id + ' .overlay-text.active').removeClass('active');
            var s = document.getElementById(emap.data('scaled'));
            s.setAttribute('class', 'mappoint-land');
            usmap.find('g').css({transform: 'translate(0,0)'});
            emap.data('scaled', false);
          });

          // Add the zooming ability to the map.
          emap.find('path').click(function (e) {
            var scaled = emap.data('scaled');

            if (scaled) {
              var s = document.getElementById(emap.data('scaled'));
              s.setAttribute('class', 'mappoint-land');
              usmap.find('g').css({transform: 'translate(0,0)'});
              $(emap).data('scaled', false);
              return;
            }

            this.setAttribute('class', 'mappoint-land mappoint-highlight');
            var r = this.getBBox();
            var w = r.width;
            var h = r.height;
            var x = r.x + w / 2;
            var y = r.y + h / 2;
            var mw = 1095;
            var mh = 748;
            var mx = mw / 2;
            var my = mh / 2;

            var sc = Math.floor(Math.min(mw / w, mh / h)) - 1;

            usmap.find('g').css({
              transform: 'translate(' + (mx - x * sc) + 'px' + ',' + (my - y * sc) + 'px) scale(' + sc + ')'
            });

            emap.data('scaled', this.id);

            // Overlay text.
            var state = this.id.replace(/^US-/, '');

            if ($('#map_' + map.id + '_' + state + '_overlay_text').length > 0) {
              $('#map_' + map.id + '_overlay_drop_shadow,#map_' + map.id + '_' + state + '_overlay_text').addClass('active');
            }
          });
        }
      } // End of default map behavior.

      if (view === 'override') {
        emap.find('path').click(function (e) {

          var s = emap.data('selected');

          if (s) {
            var p = document.getElementById(s);
            p.setAttribute('class', 'mappoint-land');
          }

          this.setAttribute('class', 'mappoint-land mappoint-highlight');

          var v = this.id;
          $('#edit-state').val(v.replace(/^US\-/, ''));

          emap.data('selected', v);
        });

        if ($('#edit-state').val()) {
          var p = document.getElementById('US-' + $('#edit-state').val());
          p.setAttribute('class', 'mappoint-land mappoint-highlight');
          emap.data('selected', p.id);
        }
      } // End of override map behavior.

      if (view === 'point') {
        emap.click(function (e) {

          var s = document.getElementById('usmap_' + map.id);
          var pt = s.createSVGPoint();
          pt.x = e.clientX;
          pt.y = e.clientY;

          var loc = pt.matrixTransform(s.getScreenCTM().inverse());

          $('#edit-pointx').val(loc.x);
          $('#edit-pointy').val(loc.y);

          var c = document.getElementById('edit-point');

          if (!c) {
            c = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
            c.id = 'edit-point';
            emap.find('g').append(c);
          }

          c.setAttribute('cx', loc.x);
          c.setAttribute('cy', loc.y);
          c.setAttribute('r', 5);
          c.setAttribute('fill', '#000');

        });
      }
    }
  };
})(jQuery);
